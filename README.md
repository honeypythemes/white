# White

White - honey.py's theme

## Install

Download honey.py

    $ wget https://github.com/libpy/honey.py/archive/master.zip
    $ unzip master.zip && mv honey.py-master yoursite

Download theme and overwrite

    $ wget https://honeypythemes.gitlab.io/white/white.zip
    $ unzip -o white.zip -d yoursite

Start server and access via http://localhost:8000/

    $ cd yoursite/_site && python -m http.server

## Demo

https://honeypythemes.gitlab.io/white/

